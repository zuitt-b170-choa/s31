// set up of dependencies
/*
-load express and mongoose packages and store them inside the express and mongoose variables respectively
- create "app" variable and store express server inside
- create a port variable and store 3000 in it
- make the app able to read and accept json and other types of data from the forms.
- connect your app to mongodb (use the on we have previously used b170 to do database)
- set up confirmation of connecting or not connecting to the database
- make the app listen to port and set up confirmation in the console that the "server is running at port xxxx"
*/

const express = require("express");
const mongoose = require("mongoose");

//to load the package that is inside the routes.js file in the repo
const taskRoute = require("./routes/taskRoutes.js")
const app = express();
const port = 3000;

mongoose.connect("mongodb+srv://gabchoa:gabchoa123@wdc028-course-booking.c9sio.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database."));

app.use(express.json());
app.use(express.urlencoded({ extended:true }));

// gives th app an access to the routes needed
// this will set the base URI once we want to access the routes under the tasks
app.use("/tasks", taskRoute)



app.listen(port, () => console.log(`Server running at ${port}`));