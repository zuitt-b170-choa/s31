// THE HOUSE OF OUR APP
// containing the functions/statements/logic to be executed once a route has been triggered/entered
// responsible for execution of CRUD operations/methods

//to give acces to the contents for tasks.js file in the models folder; meaning it can use the Task model
const Task = require("../models/task.js")

//create controller functions that respond to the routes
module.exports.getAllTasks = ()=>{
	return Task.find({}).then(result=>{
		return result;
	})
}

module.exports.createTask = (requestBody)=>{
	let newTask = new Task({
		name: requestBody.name
	})
	// saving mechanisms
	/*
		.then accepts 2 parameters (for controller functions, it's different to the previous)
			- first parameter stores the result object; if we have successfully saved the object
			- second parameter stores the error object, should there be one
	*/
	return newTask.save().then((savedTask, error)=>{
		//error handling
		if(error){
			console.log(error);
			return false;
		}
		else{
			return savedTask;
		}
	})
}


/*
	1. look for the task with the corresponding id provided in the URL
	2. delete the task
*/
module.exports.deleteTask = (taskID)=>{

	//findByIdandRemove() - finds the item to be deleted and removes it from the database; it uses id in looking for the document
	return Task.findByIdAndRemove(taskID).then((removedTask, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskID, requestBody)=>{
	return Task.findById(taskID).then((updatedTask,error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			updatedTask.name = requestBody.name
			return updatedTask.save().then((updateTask,error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return updateTask;
				}

			});
		}
	})
}

module.exports.getTask = (taskID)=>{
	return Task.findById(taskID).then((getTask, error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			return getTask;
		}
	})
}

module.exports.completeTask = (taskID, requestBody)=>{
	return Task.findById(taskID).then((result,error)=>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.status = requestBody.status
			return result.save().then((completeTask,error)=>{
				if(error){
					console.log(error);
					return false;
				}
				else{
					return completeTask;
				}
			})
		}
	})
}